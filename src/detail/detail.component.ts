import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [CommonModule],
  // templateUrl: './detail.component.html',
  template: `
    <input />
    <a (click)="changeFromChild()">Change from child</a>
    <br />
    {{ parentData }}
  `,
  styleUrl: './detail.component.scss',
})
export class DetailComponent implements OnInit, OnChanges {
  @Input() title: string = 'hello';
  @Input() parentData: string = 'hihi';
  constructor() {}
  ngOnInit(): void {}
  changeFromChild() {
    // console.log(1);
  }
  ngOnChanges(changes: SimpleChanges) {
    // console.log('changes', changes?.['title']?.currentValue);
  }
  // @Output() onChangeEv = new Event<void>();
  @Output() detailClick = new EventEmitter<void>();
}
