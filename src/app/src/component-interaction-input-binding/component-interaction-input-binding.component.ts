import {
  Component,
  DoCheck,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-component-interaction-input-binding',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './component-interaction-input-binding.component.html',
  styleUrl: './component-interaction-input-binding.component.scss',
})
export class ComponentInteractionInputBindingComponent
  implements OnInit, OnChanges
{
  @Input() backgroundColor = '#9e9e9e';
  @Input() progressColor = '#2e8b57';
  @Input() progress: number = 50;
  constructor() {}
  ngOnInit(): void {
    // console.log({ progress: this.progress });
  }
  // hoạt động khi có sự thay đổi từ parent xuống child
  ngOnChanges(changes: SimpleChanges): void {
    // console.log('changes', this.progress);
  }
}
