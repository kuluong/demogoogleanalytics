import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentInteractionInputBindingComponent } from './component-interaction-input-binding.component';

describe('ComponentInteractionInputBindingComponent', () => {
  let component: ComponentInteractionInputBindingComponent;
  let fixture: ComponentFixture<ComponentInteractionInputBindingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ComponentInteractionInputBindingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ComponentInteractionInputBindingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
