import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { interval } from 'rxjs';
import { AdultPipe } from './customPipe.pipe';

@Component({
  selector: 'app-demo-pipe',
  templateUrl: './demo-pipe.component.html',
  standalone: true,
  imports: [CommonModule, AdultPipe],
})
export class DemoPipe {
  currentDate = new Date();
  user = {
    name: 'lương',
    age: 23,
  };
  interval$ = interval(1000);
  users = [
    {
      name: 'lương',
      age: 23,
    },
    {
      name: 'tuấn',
      age: 30,
    },
    {
      name: 'luyn',
      age: 24,
    },
    {
      name: 'anh',
      age: 16,
    },
  ];
}
