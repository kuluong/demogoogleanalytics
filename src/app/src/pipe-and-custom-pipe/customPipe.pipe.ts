import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'adultPipe',
  standalone: true,
})
export class AdultPipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    return value.filter((v: any) => v.age > 18);
  }
}
