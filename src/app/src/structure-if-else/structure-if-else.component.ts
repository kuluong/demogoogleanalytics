import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-structure-if-else',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './structure-if-else.component.html',
  styleUrl: './structure-if-else.component.scss',
})
export class StructureIfElseComponent {
  @Input() showContent: boolean = true;
}
