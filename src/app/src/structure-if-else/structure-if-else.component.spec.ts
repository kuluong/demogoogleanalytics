import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureIfElseComponent } from './structure-if-else.component';

describe('StructureIfElseComponent', () => {
  let component: StructureIfElseComponent;
  let fixture: ComponentFixture<StructureIfElseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StructureIfElseComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StructureIfElseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
