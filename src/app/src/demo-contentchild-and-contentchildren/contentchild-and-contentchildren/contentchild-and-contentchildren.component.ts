import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterComponent } from '../counter/counter.component';

@Component({
  selector: 'app-contentchild-and-contentchildren',
  standalone: true,
  imports: [CommonModule, CounterComponent],
  templateUrl: './contentchild-and-contentchildren.component.html',
})
export class ContentchildAndContentchildrenComponent {
  @Input() tabs: string[] = [];
  @Output() clicked = new EventEmitter<any>();
  itemSelected: string = '';
  selectedItem(tab: string) {
    this.itemSelected = tab;
  }
}
