import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
let _counter = 1;
@Component({
  selector: 'app-counter',
  standalone: true,
  imports: [CommonModule],
  template: ` <div>Count: {{ counter }}</div> `,
})
export class CounterComponent {
  counter = _counter++;
}
