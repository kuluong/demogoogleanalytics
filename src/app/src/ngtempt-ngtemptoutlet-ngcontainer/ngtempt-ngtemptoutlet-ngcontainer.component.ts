import { Component, Input, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-ngtempt-ngtemptoutlet-ngcontainer',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './ngtempt-ngtemptoutlet-ngcontainer.component.html',
  styleUrl: './ngtempt-ngtemptoutlet-ngcontainer.component.scss',
})
export class NgtemptNgtemptoutletNgcontainerComponent {
  counter = 1;
  @Input() otherTemplate!: TemplateRef<any>;
}
