import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgtemptNgtemptoutletNgcontainerComponent } from './ngtempt-ngtemptoutlet-ngcontainer.component';

describe('NgtemptNgtemptoutletNgcontainerComponent', () => {
  let component: NgtemptNgtemptoutletNgcontainerComponent;
  let fixture: ComponentFixture<NgtemptNgtemptoutletNgcontainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NgtemptNgtemptoutletNgcontainerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NgtemptNgtemptoutletNgcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
