import { Component, Injectable, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoService } from '../service/demo.service';

@Injectable()
class CarService {
  constructor() {}
  getInfoProduct() {
    // console.log('call here !!!');
    // return 1;
  }
}

@Component({
  selector: 'app-dependency-injection-angular',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './dependency-injection-angular.component.html',
  styleUrl: './dependency-injection-angular.component.scss',
  providers: [CarService],
})
export class DependencyInjectionAngularComponent implements OnInit {
  constructor(private demoInjectTable: CarService) {}
  ngOnInit(): void {
    this.demoInjectTable.getInfoProduct();
  }
}
