import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DependencyInjectionAngularComponent } from './dependency-injection-angular.component';

describe('DependencyInjectionAngularComponent', () => {
  let component: DependencyInjectionAngularComponent;
  let fixture: ComponentFixture<DependencyInjectionAngularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DependencyInjectionAngularComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DependencyInjectionAngularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
