import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  buffer,
  bufferTime,
  debounceTime,
  defer,
  delay,
  distinct,
  distinctUntilChanged,
  distinctUntilKeyChanged,
  filter,
  find,
  first,
  from,
  fromEvent,
  fromEventPattern,
  interval,
  last,
  map,
  mapTo,
  merge,
  of,
  pipe,
  pluck,
  reduce,
  scan,
  single,
  skip,
  skipUntil,
  skipWhile,
  take,
  takeLast,
  takeUntil,
  takeWhile,
  throttle,
  throttleTime,
  timer,
  toArray,
} from 'rxjs';

@Component({
  selector: 'app-demo-observable',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './demo-observable.component.html',
  styleUrl: './demo-observable.component.scss',
})
export class DemoObservableComponent implements OnInit {
  debounceInputVal = '';
  observer = {
    next: (val: unknown) => console.log('observer value: ', val),
    error: (error: Error) => console.log('Observer error: ', error),
    complete: () => console.log('complete'),
  };
  ngOnInit(): void {
    //#################### RxJS creation  operators ####################\\
    //##### of()
    // of([1, 2, 3]).subscribe(this.observer);
    // of('1, 2, 3').subscribe(this.observer);
    // of(1,2,3,[1, 2, 3]).subscribe(this.observer);
    //#### from()
    // from([1, 2, 3]).subscribe(this.observer);
    // from([1, 2, 3, [4, 5, 6, [7, 8, 9]]]).subscribe(this.observer);
    // from('hello world').subscribe(this.observer);
    // from(Promise.resolve('hello world')).subscribe(this.observer);
    //#### fromEvent()
    // fromEvent(document, 'click').subscribe(this.observer);
    //#### fromEventPattern()
    // fromEventPattern(
    //   (addHandler) => {
    //     document.addEventListener('click', addHandler);
    //   },
    //   (remoteHandler) => {
    //     document.removeEventListener('click', remoteHandler);
    //   }
    // ).subscribe(this.observer);
    //##### interval()
    // interval(1000).subscribe(this.observer);
    //##### timer()
    // timer(1000).subscribe(this.observer);
    // timer(1000, 1500).subscribe(this.observer);

    //##### defer
    // const useDefer$ = defer(() => of(Math.random()));
    // useDefer$.subscribe(this.observer);
    // useDefer$.subscribe(this.observer);
    // useDefer$.subscribe(this.observer);

    const users = [
      {
        id: 'ddfe3653-1569-4f2f-b57f-bf9bae542662',
        username: 'tiepphan',
        count: 5,
      },
      {
        id: '34784716-019b-4868-86cd-02287e49c2d3',
        username: 'nartc',
        count: 10,
      },
    ];
    //#################### RxJS transformation  operators ####################\\
    //##### map(Fn): khác với map của javascript
    // from(users)
    //   .pipe(
    //     map((users) => {
    //       console.log(users);
    //     })
    //   )
    //   .subscribe(this.observer);

    //##### pluck
    // from(users).pipe(pluck('id')).subscribe(this.observer);

    //#### mapTo:mpa bất cứ observable nào thành giá trị fixed thành giá trị mặc định
    // merge(
    //   fromEvent(document, 'mouseover').pipe(mapTo(true)),
    //   fromEvent(document, 'mouseleave').pipe(mapTo(false))
    // ).subscribe(this.observer);

    //##### reduce: đợi cho đến khi nào  source complete tì nó mới emit giá trị cuối

    // const users$ = merge(
    //   of(users[0]).pipe(delay(2000)),
    //   of(users[1]).pipe(delay(4000))
    // );
    // users$
    //   .pipe(reduce((acc, val) => acc + val.count, 0))
    //   .subscribe(this.observer);

    //##### scan: sau mỗi lần emit 1  value, và apply một func lên value đó,, nhưng có sử dụng kèm theo kết quả trước đó
    // const user$ = merge(
    //   of(users[0]).pipe(delay(1000)),
    //   of(users[1]).pipe(delay(2000))
    // );
    // user$
    //   .pipe(scan((acc, curr) => acc + curr.count, 0))
    //   .subscribe(this.observer);
    //-> 5 15 ....

    //##### toArray:muốn lấy toàn bộ value emit bởi stream rồi lưu trữ thành một array
    // const users$ = merge(
    //   of(users[0]).pipe(delay(2000)),
    //   of(users[1]).pipe(delay(4000))
    // );
    // users$.pipe(toArray()).subscribe(this.observer);

    //##### buffer: lưu trữ dữ liệu được emit ra và đợi cho đến khi có sự kiện closingNotifier: e.g: sự kiện click
    // const interval$ = interval(1000);
    // const buffer$ = interval$.pipe(buffer(fromEvent(document, 'click')));
    // buffer$.subscribe(this.observer);

    //##### bufferTime: lưu sự kiện dữ liệu được emit ra và emit values mỗi khoảng thời  gian bufferTimeSpan (nôn na là sau bao lâu thì emit ra dữ liệu thay vì click như buffer)

    // const interval$ = interval(1000);
    // const bufferTime$ = interval$.pipe(bufferTime(2000));
    // bufferTime$.subscribe(this.observer);

    //#################### RxJS filtering  operators ####################\\

    const items = [1, 2, 3, 4, 5, 6, 7, 8];
    //#### filter()
    const filter$ = from(items).pipe(filter((x) => x > 5));
    // filter$.subscribe(this.observer); //->return 6, 7, 8

    //#### find(): trả về val đầu tiên thoả mãn điều kiện
    const find$ = from(items).pipe(find((x) => x > 5));
    // find$.subscribe(this.observer); // -> return 6

    //#### first():sẽ emit giá tị đầu tiên của 1 Observable rồi complete. first() sẽ throw err nếu observable rỗng hoặc complete trước emit
    const first$ = from(items).pipe(first());
    // first$.subscribe(this.observer); //return 1

    //#### last(): sẽ emit giá trị cuối cùng của một Observable rồi complete.last() sẽ throw err nếu observable rỗng hoặc complete trước emit
    const last$ = from(items).pipe(last());
    // last$.subscribe(this.observer); // return 8

    //#### single: emit giá trị đầu tiên thoả mãn điều kiện, single() sẽ throw error nếu có nhiều hơn 1 giá trị thoả mãn điều kiện
    const single$ = from(items).pipe(single((x) => x > 7)); // return 8
    const single1$ = from(items).pipe(single((x) => x > 5)); // return 6,7,8 -> return nhiều hơn 1 giá trị thoả mãn điều kiện -> error
    // single$.subscribe(this.observer);

    //#### take(): nhận vào một tham số(count:number) để dùng cho số lần lấy giá trị được emit từ observable
    const take$ = from(items).pipe(take(3));
    // take$.subscribe(this.observer); // return 1,2,3

    //#### takeLast(): giống take(), takeLast() sẽ lấy n giá tị cuối cùng được emit từ observable. Lưu ý, takeLast() chỉ emit khi nào Observable gốc complete, nếu như một Observable gốc là một long-live(ví dụ: interval) thì takeLast() sẽ không bao giờ emit
    const takeLast$ = from(items).pipe(takeLast(3));
    const takeLastWithLongLive$ = interval(1000)
      .pipe(takeLast(3))
      .subscribe(this.observer); //đây là một Observable long-live-> takeLast sẽ không emit giá trị
    // takeLast$.subscribe(this.observer); // return 6,7,8

    //#### takeUtil:nhận vào tham số là một Observable như là một notifier và takeUtil sẽ emit giá trị của Observable gốc cho tới khi notifier emit(như ví dụ dưới, takeUtil sẽ emit 0 ,1,2,3,4,.. liên tục, khi có sự kiện click vào thì takeUtil sẽ dừng  và complete)
    const takeUtil$ = interval(1000).pipe(
      takeUntil(fromEvent(document, 'click'))
    );
    // takeUtil$.subscribe(this.observer);

    //#### takeWhile: tương tự như takeUtil, thay vì takeUtil nhận vào tham số là một notifier thì takeWhile() sẽ nhận vào một predicate(thuộc tính)
    // takeWhile() hoạt động hiệu quả khi muốn unsubscribe từ chính giá trị mà Observable emit(internal) giống như ví dụ dưới
    const takeWhile$ = from(items).pipe(takeWhile((x) => x < 6));
    // takeWhile$.subscribe(this.observer); // return 1,2,3,4,5 ->complete

    //##### skip(): bỏ qua n giá trị ban đầu
    const skip$ = from(items).pipe(skip(4));
    // skip$.subscribe(this.observer); // return 5,6,7,8 -> complete

    //#### skipUtil:nhận vào tham số là một Observable như là một notifier và skipUtil sẽ bỏ qua giá trị của Observable gốc cho tới khi notifier emit(như ví dụ dưới
    const skipUtil$ = interval(1000).pipe(
      skipUntil(fromEvent(document, 'click'))
    );
    // skipUtil$.subscribe(this.observer); // sau 3s mới click thì giá trị sẽ là -> 4,5,6,7,8.....

    //#### skipWhile: Tương tự như skipUtil, nhưng thay vì nhận vào một notifier thì skipWhile sẽ nhận vào một predicate

    const skipWhile$ = interval(1000).pipe(skipWhile((x) => x < 5));
    // skipWhile$.subscribe(this.observer); // return  5,6,7,8,...,n

    //##### distinct():sẽ so sánh các giá trị emit từ  Observable và chỉ emit các giá trị chưa được emit qua (nôn na là những giá tri không trùng lặp)
    const distinct$ = from([1, 1, 2, 3, 4, 2, 1, 4]).pipe(distinct());
    // distinct$.subscribe(this.observer); // -> 1,2,3,4
    const distinct2$ = of(
      { age: 4, name: 'Foo' },
      { age: 7, name: 'Bar' },
      { age: 5, name: 'Foo' }
    ).pipe(distinct((p) => p.name));
    // distinct2$.subscribe(this.observer); // ->{ age: 4, name: 'Foo' },{ age: 7, name: 'Bar' },

    //#### distinctUntilChanged(): so sánh giá trị sắp được emit với giá trị vừa được emit (giá trị cuối)
    const distinctUntilChanged$ = from([1, 1, 2, 2, 1, 3, 3, 4]).pipe(
      distinctUntilChanged()
    );
    // distinctUntilChanged$.subscribe(this.observer); // -> 1,2,1,3,4
    const distinctUntilChanged2$ = of(
      { age: 4, name: 'Foo' },
      { age: 6, name: 'Foo' },
      { age: 7, name: 'Bar' },
      { age: 5, name: 'Foo' }
    ).pipe(distinctUntilChanged((a, b) => a.name === b.name));
    // distinctUntilChanged2$.subscribe(this.observer); // -> { age: 4, name: 'Foo' }, { age: 7, name: 'Bar' }, { age: 5, name: 'Foo' } -> complete

    //##### distinctUntilKeyChanged(): là short-cut của distinctUntilChanged() + keySelector
    const distinctUntilKeyChanged$ = of(
      { age: 4, name: 'Foo' },
      { age: 6, name: 'Foo' },
      { age: 7, name: 'Bar' },
      { age: 5, name: 'Foo' }
    ).pipe(distinctUntilKeyChanged('name'));
    // distinctUntilKeyChanged$.subscribe(this.observer); //{ age: 4, name: 'Foo' }, { age: 7, name: 'Bar' }, { age: 5, name: 'Foo' } -> complete

    //##### throttleTime()
    const throttleTime$ = fromEvent(document, 'mousemove').pipe(
      throttleTime(2000)
    );
    // throttleTime$.subscribe(this.observer);

    //#### throttle()
    const throttle$ = fromEvent(document, 'mousemove').pipe(
      throttle((val) => interval(2000))
    );
    // throttle$.subscribe(this.observer);

    //##### debounceTime
    const debounceInput = document.getElementById('debounceInput');

    fromEvent(debounceInput!, 'keydown')
      .pipe(pluck('target', 'value'), debounceTime(1000))
      .subscribe((val: any) => (this.debounceInputVal = val.toString()));
  }
}
