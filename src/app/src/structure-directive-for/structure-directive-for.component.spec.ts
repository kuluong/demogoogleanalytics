import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureDirectiveForComponent } from './structure-directive-for.component';

describe('StructureDirectiveForComponent', () => {
  let component: StructureDirectiveForComponent;
  let fixture: ComponentFixture<StructureDirectiveForComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StructureDirectiveForComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StructureDirectiveForComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
