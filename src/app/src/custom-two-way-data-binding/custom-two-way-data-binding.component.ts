import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-custom-two-way-data-binding',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './custom-two-way-data-binding.component.html',
  styleUrl: './custom-two-way-data-binding.component.scss',
})
export class CustomTwoWayDataBindingComponent {
  name?: string;
  data: { name: string; age: number } = { name: '', age: 1 };
}
