import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-template-variable-and-viewchild-viewchildren',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './template-variable-and-viewchild-viewchildren.component.html',
  styleUrl: './template-variable-and-viewchild-viewchildren.component.scss',
})
export class TemplateVariableAndViewchildViewchildrenComponent
  implements AfterViewInit, OnInit
{
  @Output() clicked = new EventEmitter<unknown>();

  toggle() {
    this.clicked.emit(alert('clicked'));
  }
  @ViewChild('btnRef') btnRef!: ElementRef<HTMLButtonElement>;
  @ViewChildren('btnRef') listBtnRef!: QueryList<ElementRef<HTMLButtonElement>>;
  ngOnInit(): void {
    // console.log("this's ref of element in onInit", this.btnRef);
  }
  ngAfterViewInit(): void {
    // console.log("this's ref of element in afterViewInit", this.btnRef);
    // console.log('viewChildren', this.listBtnRef);
  }
}
