import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateVariableAndViewchildViewchildrenComponent } from './template-variable-and-viewchild-viewchildren.component';

describe('TemplateVariableAndViewchildViewchildrenComponent', () => {
  let component: TemplateVariableAndViewchildViewchildrenComponent;
  let fixture: ComponentFixture<TemplateVariableAndViewchildViewchildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TemplateVariableAndViewchildViewchildrenComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TemplateVariableAndViewchildViewchildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
