import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Author } from '../../authors';

@Component({
  selector: 'app-event-binding',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './event-binding.component.html',
  styleUrl: './event-binding.component.scss',
})
export class EventBindingComponent {
  @Input() author?: Author;
  @Output() selected = new EventEmitter<Author>();
  @Output() deleteAuthor = new EventEmitter<Author>();
  handleDelete() {
    this.deleteAuthor.emit(this.author);
  }
}
