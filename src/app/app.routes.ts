import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'event-binding',
    loadComponent: () =>
      import('./src/event-binding/event-binding.component').then(
        (mod) => mod.EventBindingComponent
      ),
  },
];
