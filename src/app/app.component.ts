import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  NgModule,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { StructureIfElseComponent } from './src/structure-if-else/structure-if-else.component';
import { StructureDirectiveForComponent } from './src/structure-directive-for/structure-directive-for.component';
import { ComponentInteractionInputBindingComponent } from './src/component-interaction-input-binding/component-interaction-input-binding.component';
import { EventBindingComponent } from './src/event-binding/event-binding.component';
import { Author, authors } from './authors';
import { CustomTwoWayDataBindingComponent } from './src/custom-two-way-data-binding/custom-two-way-data-binding.component';
import { FormsModule } from '@angular/forms';
import { TemplateVariableAndViewchildViewchildrenComponent } from './src/template-variable-and-viewchild-viewchildren/template-variable-and-viewchild-viewchildren.component';
import { ContentProjectionComponent } from './src/content-projection/content-projection.component';
import { NgtemptNgtemptoutletNgcontainerComponent } from './src/ngtempt-ngtemptoutlet-ngcontainer/ngtempt-ngtemptoutlet-ngcontainer.component';
import { DependencyInjectionAngularComponent } from './src/dependency-injection-angular/dependency-injection-angular.component';
import { ContentchildAndContentchildrenComponent } from './src/demo-contentchild-and-contentchildren/contentchild-and-contentchildren/contentchild-and-contentchildren.component';
import { CounterComponent } from './src/demo-contentchild-and-contentchildren/counter/counter.component';
import { TabContentDirective } from './src/demo-contentchild-and-contentchildren/tabs/Tab-content.directive';
import { DemoPipe } from './src/pipe-and-custom-pipe/demo-pipe.component';
import { DemoObservableComponent } from './src/demo-observable/demo-observable.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    FormsModule,
    StructureIfElseComponent,
    StructureDirectiveForComponent,
    ComponentInteractionInputBindingComponent,
    EventBindingComponent,
    CustomTwoWayDataBindingComponent,
    TemplateVariableAndViewchildViewchildrenComponent,
    ContentProjectionComponent,
    NgtemptNgtemptoutletNgcontainerComponent,
    DependencyInjectionAngularComponent,
    ContentchildAndContentchildrenComponent,
    CounterComponent,
    DemoPipe,
    DemoObservableComponent,
  ],

  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements AfterViewInit, OnInit, AfterContentInit {
  @Output() clicked = new EventEmitter<HTMLButtonElement>();
  title = 'study-angular';
  ngAfterViewInit(): void {
    gtag('event', 'page_view', {
      page_title: 'title',
      page_path: 'event.urlAfterRedirects',
      page_location: ' this.document.location.href',
    });
    // console.log('after:', this.toggleBtn);
  }
  handleClick() {
    gtag('event', 'button_click111', {
      event_category: 'user_interaction',
      event_action: 'click',
      event_label: 'my_button',
      value: 11,
      non_interaction: false,
      event_callback: function () {
        console.log('Event tracked!');
      },
      transport: 'beacon',
      items: 'xxx',
      users: [{ name: 'luong' }],
    });
    // gtag('event', 'page_view', {
    //   page_title: 'title1',
    //   page_path: 'event.urlAfterRedirects',
    //   page_location: ' this.document.location.href',
    // });
  }
  //event binding
  listAuthor = authors;
  authorSelected = authors[0];
  //selected author
  onSelected(val: Author) {
    this.authorSelected = val;
  }
  //delete author
  handleDeleteAuthor(author: Author) {
    // console.log(authors.length);
    this.listAuthor = this.listAuthor.filter((au) => au.id !== author.id);
  }

  //template variable and view Child and view Children

  @ViewChild('toggleBtn')
  toggleBtn?: TemplateVariableAndViewchildViewchildrenComponent;

  // contentChild and contentChildren
  tabSelected: string = '';
  @ContentChild(TabContentDirective, { static: true, read: TemplateRef })
  explicitBody!: TemplateRef<unknown>;
  ngOnInit(): void {
    // console.log(this.toggleBtn);
  }
  ngAfterContentInit(): void {
    // console.log('contentChild', this.explicitBody);
  }
}
